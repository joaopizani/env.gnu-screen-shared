#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

BINDIR="${HOME}/.local/bin"

TMPFILESD="/etc/tmpfiles.d"
TMPFILESSCREENCFG="${DIR}/00-maybe-debianisms/etc-tmpfilesd-screen-cleanup.conf"

ACCOUNTSSERVD="/var/lib/AccountsService/users"
ACCOUNTSSERVCFG="${DIR}/00-maybe-debianisms/var-lib-accountsservice-users-guestcoder"

RCFILENAME="enable-multiuser.rc"
RCFILEPATHRAW="${DIR}/${RCFILENAME}"
RCFILEPATH="${RCFILEPATHRAW/#$HOME/\$HOME}"

SCREENRC_TRG_DEFAULT="${HOME}/.screenrc"
SCREENRC_TRG="${1:-"${SCREENRC_TRG_DEFAULT}"}"


mkdir -p "${BINDIR}"
ln -sfn "${DIR}/newkey.sh" "${BINDIR}/newkey-shared-screen.sh"
hash -r

read -e -n 1 -p 'Press "y" to create a user "guestcoder" for the single purpose of GNU screen sharing, anything else for not > ' OPTCODE
if [[ "${OPTCODE}" = "y" ]]; then
    sudo useradd -m -u 9800 -s /usr/sbin/nologin "guestcoder"
    sudo cp "${ACCOUNTSSERVCFG}" "${ACCOUNTSSERVD}/guestcoder"
fi

echo "source \"${RCFILEPATH}\"" >> "${SCREENRC_TRG}"

echo -e "\n\nGiving setuid permission to /usr/bin/screen and 755 to {,/var}/run/screen. It's needed for multiuser mode"
sudo chmod u+s "$(which screen)"
sudo chmod 755 {,/var}/run/screen  /run/screens  &>/dev/null
sudo mkdir -p "${TMPFILESD}"
sudo cp "${TMPFILESSCREENCFG}" "${TMPFILESD}/screen-cleanup.conf"

