#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" && pwd)"

GC_RUNNER="$(whoami)"
GC_USER_DEFAULT="${GC_RUNNER}"
GC_USER="${1:-"${GC_USER_DEFAULT}"}"
GC_HOME="$(getent passwd "${GC_USER}" | cut --delimiter=: --fields=6)"
GPGKEYID="${2-"notset"}"

GC_SUFFIX="shared"
GC_KEYNAME="${GC_USER}_${GC_SUFFIX}_$(date '+%Y%m%dT%H%M%S')"
GC_KEYSDIR="${DIR}/99-keypairs-dedicated";  mkdir -p "${GC_KEYSDIR}"
GC_KEYPATH="${GC_KEYSDIR}/${GC_KEYNAME}"

SCREEN_ATTACH_COMMAND="screen -r ${GC_RUNNER}/${GC_SUFFIX}"
SSH_AUTHPREFIX="command=\"${SCREEN_ATTACH_COMMAND}\",no-port-forwarding,no-X11-forwarding,no-agent-forwarding"
SSH_DIR="${GC_HOME}/.ssh"
SSH_AUTHKEYSFILE="${SSH_DIR}/authorized_keys"

PUBIP="$(wget 'http://ipecho.net/plain' -O - -q)"


if [[ "${GPGKEYID}" == "notset" ]]; then
    echo -e "\nCreating new key pair for secure shared screen access"
    ssh-keygen -C "${GC_KEYNAME}" -f "${GC_KEYPATH}"
    SSH_PUBKEY="$(cat "${GC_KEYPATH}.pub")"
else
    echo -e "\nUsing latest GPG subkey with [A]Authentication capability that can be found by searching with \"${GPGKEYID}\""
    SSH_PUBKEY="$(gpg --export-ssh-key "${GPGKEYID}")";  if [[ $? != 0 ]]; then exit $?; fi
    GC_KEYNAME="$(echo "${SSH_PUBKEY}" | cut --delimiter=' ' --fields=3)"
fi

SSH_AUTHLINE="${SSH_AUTHPREFIX} ${SSH_PUBKEY}"


echo -e "\n\nWriting new purpose-specific pubkey line to \"${SSH_AUTHKEYSFILE}\"."
[[ ${GC_USER} == ${GC_RUNNER} ]] || echo "The commands need to be run as user ${GC_USER}"
sudo -u  "${GC_USER}" mkdir -p "${SSH_DIR}";  echo -en "\n\n${SSH_AUTHLINE}" | sudo -u "${GC_USER}" tee -a "${SSH_AUTHKEYSFILE}"  &>/dev/null
echo -e  "pubkey \"${GC_KEYNAME}\" wrote to: ${SSH_AUTHKEYSFILE}\n\n"


echo -e "This is your public IP address: <PUBIP>=${PUBIP}\n\n"


echo -n "Tell clients to connect using the following command:  "
echo -e "ssh$([[ "${GPGKEYID}" == "notset" ]] && echo " -i <KEYFILE>") -t -l ${GC_USER} <PUBIP> [<PORT>] \"${SCREEN_ATTACH_COMMAND}\"\n\n"


echo -e "Start the multiuser mode by the command \":multiuser on\" (shortcut: <escape>-u)\n"

echo    "Give user \"${GC_USER}\" the READ  access in your screen by the command \":aclgrp ${GC_USER} READERS\" (shortcut: <escape>-i)"
echo -e "Give user \"${GC_USER}\" the WRITE access in your screen by the command \":aclgrp ${GC_USER} WRITERS\" (shortcut: <escape>-I)\n"

echo    "Kick user \"${GC_USER}\" with command  \":acldel ${GC_USER}\"  (shortcut: <escape>-o)"
echo    "Stop the multiuser mode by command   \":multiuser off\"      (shortcut: <escape>-U)"
echo -e "After the shared work is done, you can remove the authorization by \"rm ${SSH_AUTHKEYSFILE}\"\n"
