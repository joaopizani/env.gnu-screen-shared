gnu-screen-shared
=================

GNU Screen has A LOT of interesting applications, and I like it a lot - so much that I have spent some time [tinkering it](http://gitlab.com/joaopizani/screenrc-ftw).

One of the **most** interesting features that GNU Screen provides is **screen session sharing**.
That means you can start a screen session on your computer and **other users** can connect to that screen session to just watch or also interact with the session.
You can have a remote colleague log in to your machine via SSH using a "guest" account, for example, and _only watch_ what you are doing, with complete security.
Or you can also give the remote guest the right to _write_ on your windows. Of course, you can always kick him or return him to the _watch-only_ group...

However, setting up this "environment" in a **productive and secure** way is hard, and you have to know about SSH keys, remote command execution restriction, etc.
So I created this repo to help you setup a SSH key with the sole purpose of letting someone work with you in a shared Screen session.


One-time install
----------------

To install the contents of this repo simply run `install.sh`. You will be asked to enter sudo password for two reasons:

  1. The `screen` executable needs to be given `setuid` permission in order to allow multiuser mode (for screen sharing)
  2. (Debianism) There is a system startup file which gives "incorrect" permissions to screen executable every boot,
     and we must override this file to give the "correct" permissions (with `setuid`)

Creating a new key for a nice shared screen time
------------------------------------------------

You should run the script `newkey.sh` as the Unix user who is/will be _hosting_ the shared Screen session.
That is, if you usually use you computer (and run Screen) logged in as `batman`, then `batman` is the user which must run `newkey.sh`

You should run the script like this:

```bash
    newkey.sh <optional_guest_username> <optional_keyid>
```

Both parameters are optional:

  1. The first one indicates the account to use as the "guest" account. If you accept account creation during the install of this repo,
     then you should pass the string `guestcoder` as the first parameter of `newkey.sh`
  2. The second parameter is something that GPG can use to find a key in your keyring, the holder of which will be given permission
     to join the shared screen session. You can pass keyid or uid, and the first subkey with _Authentication_ capability will be used

If the second parameter is not passed, a new SSH priv/pub keypair will be generated,
the public part will be put into the appropriate `authorized_keys`, and you should somehow transfer the private part to your screen remote guest.

The keys generated by the script are intended to be used **temporarily**,
meaning you should **very often** generate a new pair using the script and give the private key it to whoever you want to be able to access your shared sessions.
Some comments about security:

  1. You can use a passphrase or not to encrypt the private key.
     Using one is always more secure, but regardless, **change** the key used often!
     If you decide to use a passphrase, choose, for example, something that you and your remote guest both _already_ know, but is moderately hard to guess.

  2. To "revoke access" of old keys, you just go to guest user's `authorized_keys` file and delete the lines referring to the keys you think are too old
     (Each key is identified by the date in which it was created).
     - Suggested command: `sudoedit -u guestcoder ~guestcoder/.ssh/authorized_keys`

  3. The access line added to guest user's `authorized_keys` file allows **only** to join a currently-running Screen multiuser session (named `shared`).
     It allows no shell access, no port forwarding, no X11 forwarding, no other commands, basically **nothing else**.


Quick tutorial on practical usage
---------------------------------

Enough for theory, let's put the script to work.
Suppose you want to share a Screen session running on your computer with a friend.
You want to have a separate user account called `guestcoder` for the specific purpose of working in these shared sessions.

First you need to create the `guestcoder` user, which can be done like this:

```bash
    batman@batcave:~$ sudo useradd -m guestcoder -s /bin/false
```

Notice how the shell is set to `/bin/false` so that the **only** way for the user to get access is by using the provided SSH key.
The next step is to run the script to grant `guestcoder` access to batman's shared Screen sessions:

```bash
    batman@batcave:~$ ./newkey.sh guestcoder
```

If everything goes well you can just send the private key to your friend (the most recently created key lies in this repo's root with name `sshare-current-key`),
and then you start your local Screen session, with the name `shared`

```bash
    screen -S shared
```

Also, make sure the multiuser mode in Screen is activated and that `guestcoder` has access to it:

```
    <Ctrl-a>:multiuser on
    <Ctrl-a>:acladd guestcoder
```

If you happen to be using my [awesome screenrc config](http://github.com/joaopizani/screenrc-ftw),
then the first step above is not necessary, and the second step is transformed into one of the two alternatives below, according to your taste:

```
    <Ctrl-a>:aclgrp guestcoder READERS  # OR
    <Ctrl-a>:aclgrp guestcoder WRITERS
```

That's about it! Enjoy the fun of working with multiuser GNU Screen sessions, and enjoy it **securely**.
Send me ideas or patches, I would be happy to implement any improvements to make this whole thing even easier and nicer...



Reasonable default groups of permissions for multiuser mode
-----------------------------------------------------------

GNU Screen's _multiuser_ mode is awesome (http://aperiodic.net/screen/multiuser), but
can be a pain to configure in terms of security. You might want to sometimes give people
some reasonable _reader_ rights, to watch you doing something in the terminal. Some other
times, you want you co-workers to be able to also _write_ to the terminal, but in general
you don't them to be able to change permissions, quit screen, and other _administrative_
stuff...

Well, Screen's commands that grant rights are a mess to use, and so this config includes
two groups with respective permissions:

  * **READERS** are allowed only to watch what's going on on the screen. Besides, they
    can also navigate and send messages to everybody using the "wall" command
  * **WRITERS** can also write to the screen, besides watching and navigating, of course
  * **ADMIN** only you (i.e, the user who has started the Screen session) has full
    rights and can run all Screen commands

Then, after a friend or co-worker of your has connected using an account called, let's
say, "guestaccount", you can grant the right rights (no pun intended) by issuing a
simple:

    :aclgrp guestaccount READERS

or

    :aclgrp guestaccount WRITERS

Multiuser mode is enabled by default when Screen starts, but no users are allowed to attach
(you have to allow them using the commands above) so no security risk is posed. Anyways,
if you are paranoid and want to disable multiuser mode there is a convenient keybinding
even for that! The binding is \<Escape\>-Ctrl-M
